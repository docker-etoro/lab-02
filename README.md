# Docker Workshop
Lab 02: Running commands inside the container

---

## Overview

In this lab we will explore the two ways to execute commands inside a running container (docker attach and docker exec).  
During the lab you will understand why docker exec is the defacto option to access to your running containers.


## Instructions

 - Run the application in a Docker container (detached mode) using:
```
$ docker run -d -p 3000:3000 --name app selaworkshops/npm-static-app:latest
```

 - Ensure the container is running:
```
$ docker ps
```

 - Attach to the container process using:
```
$ docker attach app
```

 - Note that you are attached to the server process, not to a terminal inside the container

 - Exit from the process by enter (multiple times):
```
$ (CTRL + C)
```

 - Check the status of the running containers:
```
$ docker ps
```

 - Show the stopped containers as well:
```
$ docker ps -a
```

 - Delete the stopped container and run a new one by using:
```
$ docker rm app
$ docker run -d -p 3000:3000 --name app selaworkshops/npm-static-app:latest
```

 - Execute the terminal (interactive) inside the container using:
```
$ docker exec -it app /bin/bash
```

 - Inspect the container filesystem:
```
$ ls -l
$ pwd
$ cd /
$ ls -l
```

 - Exit from the container terminal using:
```
$ exit
```

 - Check the running containers:
```
$ docker ps
```

 - Remove the "app" container:
```
$ docker rm -f app
```
